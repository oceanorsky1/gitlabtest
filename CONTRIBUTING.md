# To Do for this test repo
- Set up git cli or git desktop for windows
- Connect remote repo to your local file system

## Git version control flow
- local repo is on your computer
- remote repo is on the hosting platform(Gitlab)
```mermaid
graph LR;
    workingDir-->staged;
    staged-->commited;
    commited-->local;
    local(push)-->remote;
```
***

```mermaid
graph LR;
    remoteBranchName-->local;
    local-->mergeConflicts;
    mergeConflicts-->commit;
    commit-->local(stateForYourCode);
```
## Local Working
- ```git add filenames(press tab) -> git commit -m "Meaningfull_message" -> git push origin branch_name```

## Remote Working
- ```git pull origin branch_name -> git checkout branch_name```

## Create your own branch
- The moment you create yourown branch a new version control will be created
    - When you change branch using ```checkout``` the files from where you're changing will remain the same in this new branch (just delete all of them and commit change)
    - Proceeding further now when you change branch it will be an empty repository
- use ```git diff``` in any case to see wether the files are changed or not
- While uploading your code on remote system i.e gitlab (**push to your own branch only**) using ```git push origin your_branch_name```
- To delete a branch use ```git branch -D your_branch_name```
- For further help use ```git branch --help``` , it will work for all commands.
